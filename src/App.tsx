import React, {lazy, Suspense} from "react";
import {Route, Router, Switch} from "react-router";
import {createBrowserHistory} from "history";
import "./App.css";

const UserList = lazy(() => import("./components/UserList"));
const User = lazy(() => import("./components/User"));
const Film = lazy(() => import("./components/Films"));
const history = createBrowserHistory();

function App()
{
    return (
        <div className="App">
            <Router history={history}>
                <Suspense fallback={<div className="loader"/>}>
                    <Switch>
                        <Route exact path="/" component={UserList}/>
                        <Route path={"/user/:id"} component={User}/>
                        <Route path={"/film/:id"} component={Film}/>
                    </Switch>
                </Suspense>
            </Router>
        </div>
    );
}

export default App;
