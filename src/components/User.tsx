import * as React from "react";
import {useEffect, useState} from "react";
import fetchService from "../services/fetchService";
import Film from "./Film";

const User: React.FC<{ history: any, match: any }> = ({history, match}) =>
{

    const [userData, setUserData] = useState<{[key: string] : [] | string}>({});
    useEffect(() =>
    {
        fetchService(`https://ghibliapi.herokuapp.com/people/${match.params.id}`)
            .then(res =>
            {
                setUserData(res);
            });
    }, []);

    console.log(userData);
    return (
        <section className="user">
            <button onClick={history.goBack}>back</button>
            <ul>{
                Object.entries(userData).map(([key, value]) => <li key={key}>
                    <label>{key}:</label>
                    <div>
                        {key==="films" ? <Film url={`${userData.films}`}/> : (value || "-")}
                    </div>
                </li>)
            }
            </ul>
        </section>
    );
};


export default User;