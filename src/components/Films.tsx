import * as React from "react";
import {useEffect, useState} from "react";
import fetchService from "../services/fetchService";

const Films: React.FC<{ location: any, history: any }> = ({location, history}) =>
{
    const [filmsData, setFilmsData] = useState<{[key: string] : [] | string}>({});
    useEffect(() =>
    {
        fetchService(location.state.filmUrl)
            .then(res =>
            {
                setFilmsData(res);
            });
    }, []);

    return (
        <section className="user">
            <button onClick={history.goBack}>back</button>
            <ul>
                {
                    Object.entries(filmsData).map(([key, value]) => <li key={key}>
                        <label>{key}:</label>
                        {
                            Array.isArray(value) ? (
                                <div>
                                    {
                                        value.map((item: string, index: number) => <div key={index}>{item}</div>)
                                    }
                                </div>
                            ) : (value || "-")
                        }
                    </li>)
                }
            </ul>
        </section>
    );
};


export default Films;