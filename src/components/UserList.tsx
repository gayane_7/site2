import * as React from "react";
import {useEffect, useState} from "react";
import fetchService from "../services/fetchService";

const UserList: React.FC<{ history: any }> = ({history}) =>
{

    const [result, setResult] = useState<any>([]);
    const [list, setList] = useState<any>([]);
    let [filterBy, setFilterBy] = useState<any>({
        gender: "",
        hair_color: "",
        eye_color: "",
    });

    let [dropdownList, setDropdownList] = useState<any>({
        gender: [],
        hair_color: [],
        eye_color: []
    });


    useEffect(() =>
    {
        if (result.length > 0)
        {
            let filtered: any[] = filteredResult();

            let filterArray: any = {
                gender: [],
                hair_color: [],
                eye_color: []
            };

            for (let filter in filterBy)
            {
                filtered.forEach((item: any) =>
                {
                    if (item[filter] && filterArray[filter].indexOf(item[filter]) === -1)
                    {
                        filterArray[filter].push(item[filter]);
                    }
                });
            }

            setList(filtered);
            setDropdownList(filterArray);
        } else
        {
            fetchService("https://ghibliapi.herokuapp.com/people")
                .then(res =>
                {
                    setResult(res);
                });
        }
    }, [filterBy, result]);

    return (
        <>
            <div className="filters">
                <label htmlFor="gender">Gender</label>
                <select onChange={changeHandler} id="gender">
                    <option>All</option>
                    {dropdownList.gender.map((gender: string, key: number) => <option key={key}
                                                                                      selected={gender === filterBy.gender}
                                                                                      value={gender}>{gender}</option>)}
                </select>
                <label htmlFor="hair_color">Hair color</label>
                <select onChange={changeHandler} id="hair_color">
                    <option>All</option>
                    {dropdownList.hair_color.map((hairColor: string, key: number) => <option key={key}
                                                                                             selected={hairColor === filterBy.hair_color}
                                                                                             value={hairColor}>{hairColor}</option>)}
                </select>
                <label htmlFor="eye_color">Eye color</label>
                <select onChange={changeHandler} id="eye_color">
                    <option>All</option>
                    {dropdownList.eye_color.map((eyeColor: string, key: number) => <option key={key}
                                                                                           selected={eyeColor === filterBy.eye_color}
                                                                                           value={eyeColor}>{eyeColor}</option>)}
                </select>
            </div>
            <div className="content">
                {result.length > 0 ?
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Age</th>
                            <th>Eye Color</th>
                            <th>Hair Color</th>
                            <th>Films</th>
                            <th>Species</th>
                            <th>URL</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            list.map((item: any, key: number) =>
                                <tr key={key} onClick={() =>
                                {
                                    history.push(`/user/${item.id}`);
                                }}>
                                    <td>{item.id}</td>
                                    <td>{item.name}</td>
                                    <td>{item.gender}</td>
                                    <td>{item.age}</td>
                                    <td>{item.eye_color}</td>
                                    <td>{item.hair_color}</td>
                                    <td onClick={(e) =>
                                    {
                                        e.stopPropagation();
                                        history.push(`/film/${item.id}`, {filmUrl: item.films[0]});
                                    }}>{item.films}</td>
                                    <td>{item.species}</td>
                                    <td>{item.url}</td>
                                </tr>
                            )
                        }
                        </tbody>
                    </table>
                    : <span className="loader"/>}
            </div>
        </>
    );


    function changeHandler(e: any)
    {
        let {value, id} = e.target;

        if (filterBy[id] !== value)
        {
            filterBy[id] = value;
        }
        setFilterBy({...filterBy, [id]: value});
    }

    function filteredResult()
    {
        let filteredArray: any = [];
        for (let key in filterBy)
        {
            if (filterBy[key] && filterBy[key] !== "All")
            {
                if (filteredArray.length > 0)
                {
                    filteredArray = filteredArray.filter((item: any) => item[key] === filterBy[key]);
                } else
                {
                    let filtered = result.filter((item: any) => item[key] === filterBy[key]);
                    filteredArray.push(...filtered);
                }
            }
        }

        if (filteredArray.length === 0)
        {
            filteredArray = [...result];
        }

        return filteredArray;
    }
};


export default UserList;