import * as React from "react";
import {useEffect, useState} from "react";
import fetchService from "../services/fetchService";

const Films: React.FC<{ url: any }> = ({url}) =>
{
    const [filmsData, setFilmsData] = useState<{ [key: string]: [] | string }>({});
    useEffect(() =>
    {
        fetchService(url)
            .then(res =>
            {
                setFilmsData(res);
            });
    }, []);

    return (
        <ul>
            {
                Object.entries(filmsData).map(([key, value]) => <li key={key}>
                    <label>{key}:</label>
                    {
                        Array.isArray(value) ? (
                            <div>
                                {
                                    value.map((item: string, index: number) => <div key={index}>{item}</div>)
                                }
                            </div>
                        ) : (value || "-")
                    }
                </li>)
            }
        </ul>
    );
};


export default Films;